#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats

reads = []


if len(sys.argv) >= 3:
    readmax = int(sys.argv[2])
    window = int(sys.argv[3])
else:
    readmax = 50000
    window = 500 

with open(sys.argv[1]) as fp:
    for line in fp:

        parts = line.split("\t")

        if len(reads) < 1 or reads[-1][1] != int(parts[1]): # same position
            reads.append((parts[0], int(parts[1]), int(parts[2]), parts[5][0]))

        if len(reads) >= readmax:
            break

fw = [0 for _ in range(window * 2 + 1)]
ff = [0 for _ in range(window * 2 + 1)]

print("Using #" + str(len(reads)) + " reads and a " + str(window) + " window around each read")

readlen = reads[0][2] - reads[0][1]
readc = 0

lower = 0

for i, read in enumerate(reads):
    if i  % 1000 == 0:
        print("Working on read #" + str(i))

    if read[3] == '+':
        readc += 1
        lset = False

        for j, inner in enumerate(reads[lower:]):
            if inner[1] < read[1] - window:
                continue
            if inner[1] > read[1] + window:
                break

            if not lset:
                lower = j
            lset = True

            if inner[3] == '-':
                fw[inner[1] - read[1] + window]  += 1

            if inner[3] == '+':
                ff[inner[1] - read[1] + window]  += 1

    if read[3] == '-':
        for inner in reads[lower:]:
            if inner[1] < read[1] - window:
                continue
            if inner[1] > read[1] + window:
                break

            if inner[3] == '+':
                fw[read[1] - inner[1] + window]  += 1

            if inner[3] == '-':
                ff[read[1] - inner[1] + window]  += 1


#fw[window - readlen : window + readlen] =  [0 for _ in range(readlen*2)]
ff[window - readlen : window + readlen] =  [0 for _ in range(readlen*2)]


plt.plot(fw, color='blue')
plt.plot(ff, color='red')
plt.show()

f = np.argmax(fw) - window

print("Readlen: " + str(readlen))
print("Fraglen: " + str(f)) 
