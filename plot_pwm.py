#!/usr/bin/env python3

import numpy as np
import sys
from weblogolib import * 
from corebio.seq import Seq, SeqList, Alphabet, unambiguous_dna_alphabet
import math

def createlogo(counts, outfile):
    counts = [[math.log(i+1)/sum(e) for i in e] for e in counts]  # scale each position 
    counts = counts[::-1]

    counts = np.array(counts)
    logo = LogoData.from_counts(unambiguous_dna_alphabet, counts)
    format = LogoFormat(logo, LogoOptions())

    jpg = pdf_formatter(logo, format)

    with open(outfile, "wb") as out:
        out.write(jpg) 

profile = eval(sys.argv[1])

if len(sys.argv) == 3:
    outfile = sys.argv[2] 
else:
    outfile = "/tmp/nopeak_logo.pdf"

createlogo(profile, outfile)

print("written pdf to " + outfile)
