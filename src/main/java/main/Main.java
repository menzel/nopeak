package main;

import filter.GroupKMers;
import logo.Logo;
import profile.Profile;
import score.Score;
import score.Scoring;

import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {

        long startTime = System.currentTimeMillis();
        String path_sample = null; // path to read bed file


        if (args.length > 2) {
            path_sample = args[1];
        } else printusage(args);



        ////////////////////
        // Creates control csv files
        ////////////////////
        if ("PROFILE".equals(args[0])) {
            if (args.length < 4) {
                System.err.println("Expected params: PROFILE read_file path_to_genome qmer-length threads");
                System.err.println("Run example: java -jar noPeak.jar PROFILE reads.bed hg19/ 8 4");
                System.exit(1);
            }

            int threadsc = Integer.parseInt(args[4]);
            threadsc = threadsc > 24 ? 24 : threadsc; //limit max thread count to 24
            int radius = 500;


            System.out.println("[" + (System.currentTimeMillis() - startTime) + "] Building profiles for " + args[3] + "-mers for a radius of 500 bp around each read");

            ////////////////////
            // Create Profiles for control data
            ////////////////////

            Profile control = new Profile(path_sample, args[2], Integer.parseInt(args[3]), radius, threadsc);
            control.writeProfilesToFile("/tmp/profile_" + path_sample.split("/")[path_sample.split("/").length - 1] + ".csv");


        ////////////////////
        // use two profile files
        ////////////////////
        } else if ("LOGO".equals(args[0])) {
            if (args.length < 4) {
                System.err.println("Expected params: LOGO read_file control_file k-mer-length fraglen");
                System.err.println("Run example: java -jar noPeak.jar PROFILE reads.csv control.csv 8 100");
                System.exit(1);
            }

            String path_control = args[2];
            double score_cutoff = 0.6;

            int fraglen = Integer.parseInt(args[4]);


            ////////////////////
            // Get Profiles for both sample and control from files
            ////////////////////

            System.out.println("[" + (System.currentTimeMillis() - startTime) + "] Reading sample profiles file " + path_sample);
            Profile profile_s = new Profile(path_sample);

            // get parameters from profile file
            String someKmer = profile_s.getResult().entrySet().iterator().next().getKey();
            int q = someKmer.length();
            int basematch = q / 2;
            int radius = profile_s.getResult().get(someKmer).size();


            System.out.println("[" + (System.currentTimeMillis() - startTime) + "] Reading control profiles file " + path_control);
            Profile profile_c = new Profile(path_control);

            Map<String, List<Integer>> profiles_sample = profile_s.getResult();
            Map<String, List<Integer>> profiles_control = profile_c.getResult();

            int readcount_sample = profile_s.getReadcount();
            int readcount_control = profile_c.getReadcount();

            ////////////////////
            // Score each kmer from the profile by the shape of the curve
            ////////////////////

            System.out.println("[" + (System.currentTimeMillis() - startTime) + "] Scoring kmers by curves");
            Scoring scoring = new Scoring(profiles_sample, profiles_control, fraglen, readcount_sample, readcount_control);
            List<Score> scores = scoring.getScores();

            String scorefile = "Scores_" + path_sample.split("/")[path_sample.split("/").length - 1] + "_" + radius;
            System.out.println("[" + (System.currentTimeMillis() - startTime) + "] Writing scores to file ./" + scorefile);
            scoring.writeToFile(scorefile);


            ////////////////////
            // Cluster the found kmers to create sequence logos
            ////////////////////


            System.out.println("[" + (System.currentTimeMillis() - startTime) + "] Cluster kmers that overlap with at least " + basematch + " bases. Filter kmers with a score less than " + score_cutoff);
            Map<String, List<String>> groupedKmers = GroupKMers.groupKMers(scores, basematch, score_cutoff);

            groupedKmers.keySet().forEach(base -> {
                System.out.println(base.toUpperCase());
                //System.out.println(Arrays.toString(groupedKmers.get(base).toArray()));
                System.out.println(groupedKmers.get(base).size());
            });

            System.out.println("[" + (System.currentTimeMillis() - startTime) + "] Create sequence logos from clustered kmers:");

            groupedKmers.keySet().forEach(base -> {
                Logo logo = new Logo(base, scores);

                System.out.println("-----");
                System.out.println(base);
                System.out.println(logo);
                System.out.println("-----");
            });

            System.out.println("Execution time " + (int) (System.currentTimeMillis() - startTime));
        } else printusage(args);

    }

    private static void printusage(String[] args) {
       if(args.length > 0)
            System.err.println("Unknown command " + args[0]);
        System.err.println("Known modes: CONTROL, LOGO");
        System.err.println("Run example: java -jar noPeak.jar LOGO reads.csv control.csv hg19/ 8 2 100");
        System.err.println("Run example: java -jar noPeak.jar PROFILE reads.bed hg19/ 8 2 100");
        System.exit(1);
    }
}
