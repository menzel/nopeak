package logo;

import org.junit.Test;
import score.Score;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class LogoTest {

    @Test
    public void createLogoTest() throws Exception{
        List<String> seq  = Arrays.asList(  "aatt",
                                            "atta",
                                            "aagt",
                                            "annn");

        Logo logo = new Logo(seq);

    }

    @Test
    public void gabpaTest() throws Exception{
        List<Score> seq = new ArrayList<>();

		try (BufferedReader br = Files.newBufferedReader(Paths.get("/home/menzel/Desktop/THM/promotion/projekte/nopeak/tmp"))) {

			String line;
			while ((line = br.readLine()) != null) {
                String[] parts = line.split("\t");
                seq.add(new Score(parts[0],Double.parseDouble(parts[1]),Double.parseDouble(parts[2])));
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

        Logo logo = new Logo(seq.get(0).getQmer(),seq);

    }


    @Test
    public void createLogoVarTest() throws Exception{
        List<Score> seq  = Arrays.asList(  new Score("aatt", 0.5, 2.9),
                                            new Score("actt", 0.5, 2.3),
                                            new Score("gatt", 0.5, 0.5));

        Logo logo = new Logo("aatt",seq);
    }
}