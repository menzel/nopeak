package logo;

import org.apache.commons.math3.linear.*;
import score.Score;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Logo {
    private double[][] pwm; // ATGCN

    private static Map<Character, Integer> mapping = new TreeMap<>();
    private List<String> vars = new ArrayList<>();

    static {
        mapping.put('a', 3);
        mapping.put('t', 0);
        mapping.put('g', 1);
        mapping.put('c', 2);
    }

    public Logo(List<String> kmers) {


        pwm = new double[kmers.get(0).length()][5];


        for(String kmer: kmers){
            char[] charArray = kmer.toCharArray();

            for (int i = 0; i < charArray.length; i++) {
                char b = charArray[i];
                pwm[i][mapping.get(b)] += 1;
            }
        }
    }


    public Logo(String seed, List<Score> kmers) {

        setPWM(seed, kmers);
    }


    public void setPWM(String base, List<Score> kmers) {
        base = base.replaceAll("n", "");

        List<Character> bases = new ArrayList<>(mapping.keySet());
        //Map<String, Integer> known = kmers.stream().collect(Collectors.toMap(Score::getQmer, s -> (int) (Math.exp(s.getHeight()))));
        Map<String, Double> known = kmers.stream().collect(Collectors.toMap(Score::getQmer, s -> Math.log(s.getHeight())));

        for (int i = 0; i < base.length(); i++) {
            for (char c : bases) {
                for (int i1 = 0; i1 < base.length()-1; i1++) { // do not iterate over the n in base
                    for (char c1: bases) {

                        char[] basearray = base.toCharArray();

                        basearray[i] = c;
                        basearray[i1] = c1;

                        String var = String.valueOf(basearray);

                        if(!vars.contains(var)) //TODO fix loops
                            vars.add(var);
                    }
                }
            }
        }



        double[][] nums = new double[vars.size()][4 * base.length()];

        int pos = 0;
        for(String var: vars) {
            char[] basearray = var.toCharArray();

            for (int i2 = 0; i2 < basearray.length; i2++) {
                char v = basearray[i2];
                nums[pos][(i2 * 4) + mapping.get(v)] = 1;
            }
            pos++;
        }

        RealMatrix coefficients = new Array2DRowRealMatrix(nums);
        DecompositionSolver solver = new SingularValueDecomposition(coefficients).getSolver();

        double[] scores =  new double[vars.size()];
        for (int i = 0; i < vars.size(); i++) {

            String seq = vars.get(i);

            if (kmers.contains(new Score(seq, 0, 0)))
                scores[i] = Math.log(kmers.get(kmers.indexOf(new Score(seq, 0, 0))).getHeight());
            else
                scores[i] = 0;
        }

        RealVector constants = new ArrayRealVector(scores, false);
        RealVector solution = solver.solve(constants);

        int j = 0;
        double[][] M;
        M = new double[solution.toArray().length/4][4];

        for(int i = 0; i < solution.toArray().length; i++){
            M[j][i%4] = solution.toArray()[i];

            if((i+1) % 4 == 0 )
                j++;
        }

        int top =  0;
        for(Score score: kmers){
            System.out.println(score.getQmer() + " " + score.getHeight() + " " + Math.exp(get_score(M,score.getQmer())));

            if(top++ > 1000) break;
        }
    }

    private double get_score(double[][] M, String seq) {

        char[] charArray = seq.toCharArray();
        double val = 0;
        for (int i = 0; i < charArray.length; i++) {
            char c = charArray[i];

            val += M[i][mapping.get(c)];
        }
        return val;
    }

    @Override
    public String toString(){

        StringBuilder r = new StringBuilder("[");

        for (double[] aPwm : pwm) {
            r.append("[");

            for (int i = 0; i < 5; i++) {
                r.append(aPwm[i]);
                r.append(",");
            }

            r.append("],");
        }

        r.append("]");

        return r.toString();
    }

    public String otherToString(){
        String r = "";

        r += ("a\tt\tg\tc\tn\n");

        for(int j = 0; j < pwm.length; j++) {
            r += j + "\t";
            for (int i = 0; i < 5; i++) {
                r += pwm[j][i];
                r += "\t";
            }
            r += "\n";
        }

        return r;
    }


    public double[][] getPwm(){
        return pwm;
    }

    /**
     * Returns a shifted version of the given qmer.
     * E.g. (2,"aaaa",2) returns "nnaaaann"
     *
     * @param prev - count of n's before the qmer
     * @param qmer - qmer sequence
     * @param post - count of n's after the qmer
     *
     * @return qmer with prev n's before the qmer String and pos n's after
     */
    private static String shift(int prev, String qmer, int post) {
        return String.join("", Collections.nCopies(prev, "n")) + qmer + String.join("", Collections.nCopies(post, "n"));
    }


}
